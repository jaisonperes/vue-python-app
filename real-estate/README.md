# real-estate

> One to rule them all

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Using Docker

```
docker-compose up
```
