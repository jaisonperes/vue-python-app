import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    token: null
  },
  mutations: {
    // Uptade user on state
    updateUser: (state, user) => {
      state.user = user
      if (user === null) {
        localStorage.removeItem('token')
      }
    },
    // Update token on state
    updateToken: (state, token) => {
      state.token = token
      window.localStorage.setItem('token', token)
    } 
  },
  actions: {
    logout: ({commit}) => {
      commit('updateUser', null)
    }
  },
  getters: {
    getUser: (state) => {
      return state.user
    },
    getToken: (state) => {
      return state.token
    }
  }
})