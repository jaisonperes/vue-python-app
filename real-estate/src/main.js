import Vue from 'vue'
import axios from 'axios'
import vueHeadful from 'vue-headful'
import App from './App.vue'
import router from './router'
import store from './store'
import btn from './components/ui/btn/Btn'
import vInput from './components/ui/input/VInput'
import card from './components/ui/card/Card'
import cardMain from './components/ui/card/CardMain'
import item from './components/ui/item/Item'
import itemMain from './components/ui/item/ItemMain'
import itemSide from './components/ui/item/ItemSide'
import scrollArea from './components/ui/scroll/ScrollArea'
import notify from './components/ui/notify/Notify'
import vTransition from './components/ui/transition/vTransition'
import VueMaterialIcon from 'vue-material-icon'
import { VueMaskDirective } from 'v-mask'
import './registerServiceWorker'

// Plugin to customize title of navigator tab
Vue.use(vueHeadful)

// Preset headers to future calls
Vue.prototype.$axios = axios.create({
  baseURL: process.env.API || 'http://localhost:8000',
  headers: {
    'Authorization': 'Basic YWRtaW46YWRtaW4=',
    'Cache-Control': 'no-cache'
  }
})
// Preset toke on headers to future authorizations
Vue.prototype.$axios.interceptors.request.use(function (config) {
  let token = localStorage.getItem('token')
  if (token) {
    config.headers['Authorization'] = `Bearer ${ token }`
  } else {
    config.headers['Authorization'] = `Basic YWRtaW46YWRtaW4=`
  }
  return config
})
Vue.config.productionTip = false

Vue.component(VueMaterialIcon.name, VueMaterialIcon)
Vue.component('btn', btn)
Vue.component('vInput', vInput)
Vue.component('card', card)
Vue.component('cardMain', cardMain)
Vue.component('item', item)
Vue.component('itemMain', itemMain)
Vue.component('itemSide', itemSide)
Vue.component('scrollArea', scrollArea)
Vue.component('vTransition', vTransition)
Vue.component('notify', notify)
Vue.component('vueHeadful', vueHeadful)

Vue.directive('mask', VueMaskDirective);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
