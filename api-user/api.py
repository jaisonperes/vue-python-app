"""
Root file to Reales Rent User API

The resoucers available should be defined into settings.py
"""

from os import environ
from datetime import datetime
import argparse
import re
from hashlib import md5
from eve.auth import TokenAuth
from secrets import token_hex
import json
import logging
import traceback
from math import ceil
from ast import literal_eval
from dotenv import load_dotenv, find_dotenv
from eve import Eve
from eve.auth import BasicAuth
from eve.auth import requires_auth
from eve.io.mongo import MongoJSONEncoder
from flask import request, abort, jsonify
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from bson.objectid import ObjectId
from eve_healthcheck import EveHealthCheck
from deepdiff import DeepDiff

# pylint: disable=ungrouped-imports
import bson
# pylint: enable=ungrouped-imports

# pylint: disable=unused-import
import settings
try:
    load_dotenv(find_dotenv())
except Exception as exception:
    pass

MONGO_USER = environ.get("MONGO_USER")
MONGO_PASS = environ.get("MONGO_PASS")

class MyBasicAuth(TokenAuth):
    """ Module to validate the auth user to access the API data"""

    def check_auth(self, token, allowed_roles, resource, method):
        auth = False
        if token:
            user_id = token[:24]
            token = token[24:]
            if len(user_id) == 24:
                auth = app.data.pymongo().db.tokens.find_one({
                    "origin_id": ObjectId(user_id),
                    "token": token
                })
        return bool(auth)

# pylint: disable=method-hidden
class JSONEncoder(json.JSONEncoder):
    """ class to enconde json with bson and datetime """

    def default(self, o):
        """
            parse objet to json
        """
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)
# pylint: enable=method-hidden


app = Eve(auth=MyBasicAuth)
app.logger.setLevel(logging.INFO)
app.name = 'users'
app.json_encoder = MongoJSONEncoder
CORS(app)
BCRYPT = Bcrypt(app)
USER_HOST = 'users'
EveHealthCheck(app, '/healthcheck')

# pylint: disable=invalid-name
p = argparse.ArgumentParser()

p.add_argument('-p', '--port', type=int, default=8000, required=False)
# pylint: enable=invalid-name

@requires_auth('home')
def before_insert(documents):
    """Generate a hash for each user document on insert using bcrypt"""
    for doc in documents:
        doc['email'] = doc['email'].lower()
        if ('auth' in doc and
                'password' in doc['auth']):
            doc['auth']['password'] = BCRYPT.generate_password_hash(
                doc['auth']['password']).decode()
        else:
            abort(422, description='Auth incorrect, requires at least one field')

#pylint: disable=too-many-branches
@app.route('/user/auth', methods=['POST'])
@requires_auth('home')
def authenticate():
    """ Authenticate user """
    nickname = ''
    try:
        argumentos = json.loads(request.data.decode('utf-8'))
        nickname = str(argumentos['nickname'])
        user = app.data.pymongo().db.users.find_one({'nickname': nickname})
        result = {'result': False, 'error': 'Wrong password -'}
        if 'pswdtk' in request.headers:
            if (BCRYPT.check_password_hash(user['auth']['password'],
                                            request.headers['pswdtk'])):
                token = token_hex(256)
                app.data.pymongo().db.tokens.insert_one({
                    "origin_id": user["_id"],
                    "token": token
                })
                result = {
                    "token": str(user["_id"]) + token
                }
        return jsonify(result)
    except Exception:
        print(traceback.format_exc())
    abort(401, description='Wrong password +')
#pylint: enable=too-many-branches

# --- Main program ---
# pylint: disable=no-member
app.on_insert_user_admin += before_insert
app.on_fetched_resource_auth += authenticate
# pylint: enable=no-member

if __name__ == '__main__':
    # the default log level is set to WARNING, so
    # we have to explictly set the logging level
    # to INFO to get our custom message logged.
    ARGS = p.parse_args()
    app.run(debug=True, host='0.0.0.0', port=ARGS.port)
