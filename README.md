# Vue App with Python Apis
## Ambiente de Desenvolvimento

### Dependências
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [MongoDB](https://docs.mongodb.com/manual/administration/install-on-linux/)
- [Real Estate Web App](https://gitlab.com/jaisonperes/real-estate)
- [Auth Api](https://gitlab.com/jaisonperes/api-reales-auth)
- [User Api](https://gitlab.com/jaisonperes/api-reales-user) 

#### Api Auth (api-auth)
Serviço de autenticação que gera o token para o usuário.

###### Python
 ```
 mkvirtualenv -p /usr/bin/python3.6 api-auth
 pip3 install -r requirements.txt 
 python3 api.py
 ```
###### Docker
  ```
 docker-compose up
 ```
###### Testes
```
coverage run --source=. -m unittest discover -s test
```

#### Api User (api-user)
Serviço para gerenciamento de informações de usuário. É necessário possuir um token.

###### Python
 ```
 mkvirtualenv -p /usr/bin/python3.6 api-user
 pip3 install -r requirements.txt 
 python3 api.py
 ```
###### Docker
 ```
 docker-compose up
 ```
###### Testes
```
coverage run --source=. -m unittest discover -s test
```

#### Web App (real-estate)
Aplicação web para cadastro, login e edição de informações do usuário.
###### Docker
```
docker-compose up 
```

###### Acesse a aplicação web no http://localhost:8080
