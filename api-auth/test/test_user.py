import json
from test_api import basicEveTest


class TestUserAPI(basicEveTest):

    """Basic User endpoint test

    Should test the basic endpoint of the user
    """
    
    def test_login_user(self):
        self.headers['pswdtk'] = self.password
        response = self.test_client.post(
            '/user/auth', headers=self.headers, data=json.dumps({'nickname': self.valid_user['nickname']}))
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.data.decode("utf-8"))
        self.assertEqual(len(response['token']), 536)
        self.headers['pswdtk'] = '123123fsdfs'
        response = self.test_client.post(
            '/user/auth', headers=self.headers, data=json.dumps({'nickname': self.valid_user['nickname']}))
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.data.decode("utf-8"))
        self.assertFalse(response['result'])
        self.assertEqual(response['error'], 'Wrong password')
